#include "Matrix.h"


Matrix::Matrix(int rowCount, int colCount) :_rowCount(rowCount), _colCount(colCount)
{
	_M = new double*[_rowCount];
	for (int i = 0; i < _rowCount; i++)
	{
		_M[i] = new double[_colCount];
		for (int j = 0; j < _colCount; j++)
		{
			_M[i][j] = 0;
		}
	}
}
Matrix::Matrix(const Matrix & other) : _rowCount(other._rowCount), _colCount(other._colCount)
{
	_M = new double*[_rowCount];
	for (int i = 0; i < _rowCount; i++)
	{
		_M[i] = new double[_colCount];
		for (int j = 0; j < _colCount; j++)
		{
			_M[i][j] = other._M[i][j];
		}
	}
}

Matrix::~Matrix()
{
	if (_M != NULL)
	{
		for (int i = 0; i < _rowCount; i++)
		{
			delete[] _M[i];
		}
		delete[] _M;
		_M = NULL;
	}
}
Matrix& Matrix::operator = (const Matrix & other)
{
	this->~Matrix();
	_rowCount = other._rowCount;
	_colCount = other._colCount;

	_M = new double*[_rowCount];
	for (int i = 0; i < _rowCount; i++)
	{
		_M[i] = new double[_colCount];
		for (int j = 0; j < _colCount; j++)
		{
			_M[i][j] = other._M[i][j];
		}
	}

	return *this;
}

Matrix& Matrix::operator=(double value){

	for (int i = 0; i < _rowCount; i++)
	{

		for (int j = 0; j < _colCount; j++)
		{
			_M[i][j] = value;
		}
	}
	return *this;
}

Matrix Matrix::operator+(const Matrix & other){

	if (_rowCount == other._rowCount && _colCount == other._colCount)
	{
		Matrix result(*this);
		for (int i = 0; i < _rowCount; i++)
		{
			for (int j = 0; j < _colCount; j++)
			{
				result._M[i][j] += other._M[i][j];
			}
		}
		return result;
	}
	else
	{
		throw "Matrix size not equal";
	}
}

Matrix operator-(const Matrix & o1, const Matrix & o2)
{
	if (o1._rowCount == o2._rowCount && o1._colCount == o2._colCount)
	{
		Matrix result(o1);
		for (int i = 0; i < o1._rowCount; i++)
		{
			for (int j = 0; j < o1._colCount; j++)
			{
				o1._M[i][j] -= o2._M[i][j];
			}
		}
		return result;
	}
	else
	{
		throw "Matrix size not equal";
	}
}

double * Matrix::operator[](int i){
	return _M[i];
}

double& Matrix::operator ()(int row, int col){
	return _M[row][col];
}

double& Matrix::operator ()(int col)
{
	return _M[1][col];
}

Matrix&	Matrix::operator++(){
	for (int i = 0; i < _rowCount; i++)
	{
		for (int j = 0; j < _colCount; j++)
		{
			_M[i][j] ++;
		}
	}
	return *this;
}
Matrix Matrix::operator++(int){
	Matrix oldValue = *this;
	for (int i = 0; i < _rowCount; i++)
	{
		for (int j = 0; j < _colCount; j++)
		{
			_M[i][j] ++;
		}
	}
	return oldValue;
}

Matrix::operator double()
{
	return _M[0][0];
}

ostream & operator<<(ostream & out, const Matrix & m){
	for (int i = 0; i < m._rowCount; i++)
	{
		for (int j = 0; j < m._colCount; j++)
		{
			out << m._M[i][j] << " ";
		}
		out << endl;
	}
	return out;
}
istream & operator>>(istream & in, const Matrix & m){
	for (int i = 0; i < m._rowCount; i++)
	{
		for (int j = 0; j < m._colCount; j++)
		{
			in >> m._M[i][j];
		}
	}
	return in;
}