#pragma once
#include <string>

using namespace std;

class GameObject
{
public:
	GameObject();
	virtual ~GameObject();
	virtual string toString() const;
};




