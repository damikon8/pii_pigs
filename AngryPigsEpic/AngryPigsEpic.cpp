// AngryPigsEpic.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Position.h"
#include "MapSpot.h"

#include <iostream>
using namespace std;
#include "GameObject.h"

int _tmain(int argc, _TCHAR* argv[])
{
	Position p1(1,2);
	Position p2(2, 3 ,0 );

	MapSpot ms1(p1, NULL,"First Spot");
	MapSpot ms2(p2, &ms1,"Second Spot");

	cout << p1.GetX() << endl;
	cout << ms1.getOutput(0)->getPosition().GetY() << endl;

//	GameObject2 g2;

//	cout  << g2.toString().c_str() << endl;
	system("pause");
	return 0;
}

