#include "GameCharacter.h"


GameCharacter::GameCharacter()
{
}


GameCharacter::~GameCharacter()
{
}

AtackResult GameCharacter::Atack(GameCharacter * characterToAtack){
//	Weapon * realWeapon = (Weapon)_weapon; 
//	realWeapon.Atack(characterToAtack);
	return AtackResult();
}
AtackResult GameCharacter::RunMagicSkill(GameCharacter * characterToAtack){ return AtackResult(); }
void GameCharacter::UsePotion(GameObject *){}

void GameCharacter::SetHealth(int value){ _health = value; }
void GameCharacter::SetLevel(int value){ _level = value; }
void GameCharacter::SetAtack(int value){ _atack = value; }
void GameCharacter::SetDefense(int value){ _defense = value; }
void GameCharacter::SetWeapon(GameObject* value){ _weapon = value; }
void GameCharacter::SetArmor(GameObject* value){ _armor = value; }
void GameCharacter::SetShield(GameObject* value){ _shield = value; }

int GameCharacter::GetHealth() const{ return _health; }
int GameCharacter::GetLevel() const{ return _level; }
int GameCharacter::GetAtack() const{ return _atack; }
int GameCharacter::GetDefense() const{ return _defense; }
GameObject* GameCharacter::GetWeapon() const{ return _weapon; }
GameObject* GameCharacter::GetArmor() const{ return _armor; }
GameObject* GameCharacter::GetShield() const{ return _shield; }
